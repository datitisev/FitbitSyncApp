import sys, subprocess, time
import logging
from multiprocessing import Pool;
from PyQt5.QtWidgets import QApplication, QWidget, QDesktopWidget, QLabel, QPushButton, QVBoxLayout

logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

def execute(command):
    cmd = command.split(' ')
    pipe = subprocess.PIPE

    return subprocess.Popen(cmd, stdout=pipe, stderr=pipe, universal_newlines=True)


class FitbitSync(QWidget):
    error = None
    device = None

    def __init__(self):
        super(FitbitSync, self).__init__()

        self.initUI()

    def initUI(self):
        self.setWindowTitle('Fitbit Sync')
        self.resize(800, 480)
        self.center()

        # GUI items
        self.label = QLabel('', self)
        button = QPushButton(self)
        button.setText('Re-try')
        button.clicked.connect(self.getDevice)
        button.setHidden(True)

        self.button = button;

        vbox = QVBoxLayout()
        vbox.addWidget(self.label)
        vbox.addWidget(self.button)

        self.setLayout(vbox)
        self.show()

        # Get devices

        self.getDevice()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def getDevice(self):
        time.sleep(0.5)

        self.button.setHidden(True)
        proc = execute('galileo')
        result = ''

        while proc.poll() is None:
            result += proc.stdout.read()
            result += proc.stderr.read()
            self.label.setText(result);
            time.sleep(0.5)

        result += proc.stdout.read()
        result += proc.stderr.read()
        self.label.setText(result)

        exit_code = proc.poll()
        result = result.strip()

        self.button.setHidden(False)

        self.device = Device(result.split('-')[1].strip()) if len(result) > 5 and 'not found' not in result else None


class Device():
    name = None

    def __init__(self, name):
        self.name = name


def main():
    app = QApplication(sys.argv)
    fitbitSync = FitbitSync()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
